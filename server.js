//Importar las bibliotecas
var express 			= require("express");
var app 				= express();
var fs 					= require('fs');
var port 				= 3000;
var app 				= express();
var bodyParser 			= require('body-parser');
var mysql 				= require('mysql');
var aboutController	  	= require("./Controllers/aboutController.js");
var inicioController  	= require("./Controllers/inicioController.js");
var registroController	= require("./Controllers/registroController.js");
var ofrecerController	= require("./Controllers/ofrecerController.js");
var contactoController	= require("./Controllers/contactoController.js");
var baseDeDatos			= require("./Models/baseDeDatos.js");
var miPerfilController	= require("./Controllers/miPerfilController.js");

//Asignar la dirección de nuestros recursos públicos

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//Asignar ejs como nuestro motor visual
app.set("view engine", "ejs");

//controllers
baseDeDatos.run(mysql);
aboutController.run(app);
inicioController.run(app, mysql);
registroController.run(app);
ofrecerController.run(app);
contactoController.run(app);
miPerfilController.run(app);

//Montar el server
app.listen(port);

//Mostrar mensaje
console.log("Aplicación arriba en http://localhost:" + port); 